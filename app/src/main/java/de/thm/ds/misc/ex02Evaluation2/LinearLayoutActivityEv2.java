package de.thm.ds.misc.ex02Evaluation2;

import android.os.Bundle;

import de.thm.ds.misc.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class LinearLayoutActivityEv2 extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setContentView(R.layout.ex02_evaluation_2);
    }
}