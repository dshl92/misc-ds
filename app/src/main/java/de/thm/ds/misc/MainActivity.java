package de.thm.ds.misc;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import de.thm.ds.misc.ex02Evaluation1.LinearLayoutActivityEv1;
import de.thm.ds.misc.ex02Evaluation2.LinearLayoutActivityEv2;
import de.thm.ds.misc.ex02LinearLayout.LinearLayoutActivity;
import de.thm.ds.misc.ex03FrameLayout.FrameLayoutActivity;
import de.thm.ds.misc.ex04ConstraintLayout.ConstraintLayoutActivity;
import de.thm.ds.misc.ex16FloatingActionButton.FloatingActionActivity;

public class MainActivity extends AppCompatActivity {

    private static final Class[] examples = new Class[] {
            LinearLayoutActivity.class,
            LinearLayoutActivityEv1.class,
            LinearLayoutActivityEv2.class,
            FrameLayoutActivity.class,
            ConstraintLayoutActivity.class,
            FloatingActionActivity.class
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        for (Class example : examples) {
            Button button = new Button(this);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            button.setLayoutParams(lp);
            String[] splitPackage = example.getPackage().getName().split("\\.");
            button.setText(splitPackage[splitPackage.length - 1]);
            button.setAllCaps(false);
            button.setOnClickListener(new StartExampleOnClickListener(example));
            layout.addView(button);
        }
        setContentView(layout);
    }

    private class StartExampleOnClickListener implements View.OnClickListener {
        private Class example;

        public StartExampleOnClickListener(Class example) {
            this.example = example;
        }

        @Override
        public void onClick(View v) {
            startActivity(new Intent(MainActivity.this, example));
        }
    }
}
