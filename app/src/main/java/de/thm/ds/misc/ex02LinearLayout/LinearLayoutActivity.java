package de.thm.ds.misc.ex02LinearLayout;

import android.os.Bundle;

import de.thm.ds.misc.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class LinearLayoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setContentView(R.layout.ex02_linear_layout);
    }
}
